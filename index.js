//Cargar envc
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const avenger = require('./api/routes/avengers');
const genero = require('./api/routes/generos');
const sys_idioma = require('./api/routes/sys_idiomas');
const sys_label = require('./api/routes/sys_labels');
const sys_label_idioma = require('./api/routes/sys_label_idiomas');
//Database
const db = require('./api/config/database');

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json());

app.use((req, res, next) => {
    let language = req.headers['accept-language'];
    if (language != 'en') {
        language = 'es';
    }
    req.lang = language;
    return next();
})

app.use('/docs', express.static(__dirname + '/src/documentacion/docs'));

// test db connection
app.use(async (req, res, next) => {
    try {
        await db.authenticate();
        return next();
    } catch (error) {
        console.log(error);
        res.status(500);
        res.json({
            valido: false,
            error: {
                mensaje: 'Unexpected error'
            },
            info: error
        });
        return;
    }
})

// Avenger routes
app.use('/avengers', avenger);
app.use('/generos', genero);
app.use('/sys_idiomas', sys_idioma);
app.use('/sys_labels', sys_label);
app.use('/sys_label_idiomas', sys_label_idioma);


const PORT = process.env.PORT || 8081;

app.listen(PORT, console.log(`Server started on port ${PORT}`));



