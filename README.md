Crear una entidad nueva dentro del API basada en los Avengers. Se requiere crear los siguientes servicios:

POST - Crear un Avenger. Deberá recibir todos los datos pertinentes y validarlos, el ID no se debe enviar dado que es genero por la base de datos.
PUT - Actualizar un Avenger. Debera recibir todos los datos pertinentes mas el ID del avenger que se va a actualizar.
DELETE - Borrar un Avenger de la base de datos. Debera recibir el ID del avenger que se quiere borrar de base de datos.
GET - Listar/filtar Avengers
Filtrar por ID
Filtrar por nombre
Filtrar por rango de edades
Filtrar por genero
El modelo Avenger deber ser el siguiente:

Id
Nombre
Descripción
Edad
Genero (Puede ser un string)
Imagen // esta imagen deberá ser subida a un S3 y guardar su dirección en base de datos. Esta imagen vendra en base64. El GET de Avengers debe enviar un URI con la imagen.
Se debe hacer un Middleware de validación que solo deje usar estos endpoints de avenger a request que tenga un header x-api-key con el key especificado por variable de entorno. Es decir si el secret es 1232KAOKC y no se manda el header de x-api-key o el secret no es el mismo (1232KAOKC ) el middleware debería devolver un error de fallo de autorización.

Referencia de Mockup: https://xd.adobe.com/spec/1636f04f-089d-4cf7-5cac-a7cd14b8b84f-6007/