CREATE DATABASE  IF NOT EXISTS `avengers_pps` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `avengers_pps`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: avengers_pps
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `avengers`
--

DROP TABLE IF EXISTS `avengers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avengers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_genero` int(11) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `imagen` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `avengers_fk1_idx` (`id_genero`),
  CONSTRAINT `avengers_fk1` FOREIGN KEY (`id_genero`) REFERENCES `genero` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avengers`
--

LOCK TABLES `avengers` WRITE;
/*!40000 ALTER TABLE `avengers` DISABLE KEYS */;
INSERT INTO `avengers` VALUES (1,2,'Gamora ',35,'https://areajugones.sport.es/wp-content/uploads/2019/07/Gamora.jpg');
/*!40000 ALTER TABLE `avengers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_label` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `genero_fk1_idx` (`id_label`),
  CONSTRAINT `genero_fk1` FOREIGN KEY (`id_label`) REFERENCES `sys_label` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_idioma`
--

DROP TABLE IF EXISTS `sys_idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_idioma` (
  `id` varchar(5) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL COMMENT '0-deshabilitado\n1-habilitado',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_idioma`
--

LOCK TABLES `sys_idioma` WRITE;
/*!40000 ALTER TABLE `sys_idioma` DISABLE KEYS */;
INSERT INTO `sys_idioma` VALUES ('en','ingles',1),('es','español',1);
/*!40000 ALTER TABLE `sys_idioma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_label`
--

DROP TABLE IF EXISTS `sys_label`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_label`
--

LOCK TABLES `sys_label` WRITE;
/*!40000 ALTER TABLE `sys_label` DISABLE KEYS */;
INSERT INTO `sys_label` VALUES (1),(2);
/*!40000 ALTER TABLE `sys_label` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_label_idioma`
--

DROP TABLE IF EXISTS `sys_label_idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_label_idioma` (
  `id_label` int(11) NOT NULL,
  `id_idioma` varchar(5) NOT NULL,
  `traduccion` longtext,
  PRIMARY KEY (`id_label`,`id_idioma`),
  KEY `sys_label_idioma_fk1_idx` (`id_idioma`),
  CONSTRAINT `sys_label_idioma_fk1` FOREIGN KEY (`id_idioma`) REFERENCES `sys_idioma` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sys_label_idioma_fk2` FOREIGN KEY (`id_label`) REFERENCES `sys_label` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_label_idioma`
--

LOCK TABLES `sys_label_idioma` WRITE;
/*!40000 ALTER TABLE `sys_label_idioma` DISABLE KEYS */;
INSERT INTO `sys_label_idioma` VALUES (1,'en','Male'),(1,'es','Masculino'),(2,'en','Female'),(2,'es','Femenino');
/*!40000 ALTER TABLE `sys_label_idioma` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-07 10:21:22
