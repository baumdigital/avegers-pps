'use strict';
//config
const ConfigAWSSDK = { // credenciales para usar el sdk de aws
  accessKeyId: process.env.AWS_SDK_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SDK_SECRET_ACCESS_KEY,
  region: process.env.AWS_SDK_REGION,
};
const S3AWSConfig = { // configuracion del bucket a donde se van a subir las imagnes/archivos
        url :  process.env.S3_URL? process.env.S3_URL : 'https://s3.amazonaws.com/',
        version: '2006-03-01',
        bucket: process.env.S3_BUCKET? process.env.S3_BUCKET : 'bucket-baum-loyalty',
};
//Packages
const AWS = require('aws-sdk');

class Storage {
    constructor(s3Config = S3AWSConfig, awsSdk = ConfigAWSSDK){
        if(!s3Config|| !awsSdk){
            throw new Errow('[Storage] Invalid constructor params');
        }
        this.s3Bucket = new AWS.AWS({  
            credentials: awsSdk,
            params: {
                Bucket: s3Config.bucket,
                ACL: ACL
            } 
        });
        this.s3Url = s3Config.url;
        this.s3Bucket = s3Config.bucket;
        //functions
        this.saveFile = this.saveFile.bind(this);
        this.deleteFile = this.deleteFile.bind(this);
        this.imageBase64toUrlS3 = this.imageBase64toUrlS3.bind(this);
        this.getImageTypeFromBase64 = this.getImageTypeFromBase64.bind(this);
    };
    /**
     * [saveFile description]
     * @param  {[string/buffer]} content     [description]
     * @param  {[string]} name        [description]
     * @param  {[string]} contentType [description]
     * @return {[promise]}             [description]
     */
    async saveFile(content, name, contentType, direccionBucket = ""){
        try {
            let buf = new Buffer(content, 'base64')
            let data = {
                Key: `${direccionBucket}${name}`,
                Body: buf,
                ContentEncoding: 'base64',
                ContentType: contentType
            };
            const result = await this.s3Bucket.putObject(data).promise();
            console.log('succesfully uploaded the image!');
            return result;
        } catch (error) {
            console.log(`Error guardado imagen ==> ${error}`)
            return null;
        }
    };
    /**
     * [deleteImage description]
     * @param  {[string]} file_dir [description]
     * @return {[promise]}          [description]
     */
    async deleteFile(file_dir){
        try{
            let params = {
                Key: file_dir
            };
            const result = await s3Bucket.deleteObject(params).promise();
            console.log(`File deleted successfully ${JSON.stringify(result)}`);
            return true;
        }catch(error){
            console.log(`Error borrando imagen ==> ${error}`)
            return false;
        }
    };
    async imageBase64toUrlS3 (imgBase64, key, direccionBucket = '',url = '') {
        try{
            if (!imgBase64 || imgBase64 === '') {
                return '';
            }
    
            //si ya hay una imagen
            if (imgBase64.indexOf('https') != -1) {
                return imgBase64; 
            }
    
            //la url para las imagenes de perfil
    
            //se obtiene tipo de imagen, y el data base64 limpio
            let imageTypeAux;
            let result = image_type(imgBase64);
    
            if (result.type == '') {
                 // si no es alguno de estos tipos
                return;
            }
    
            imageTypeAux = result.type;
            imgBase64 = result.imgBase64;
    
            //generamos un nombre de archivo
            let fileName = key + '.' + imageTypeAux;
            //guardamos la imagen en el s3
            const resultS3 = await this.saveImage(imgBase64, fileName, 'image/' + imageTypeAux, direccionBucket);
            return  (resultS3)? (url  + "/" + fileName) : null;
        }catch(error){
            console.log(error);
            return null;
        }
    
    };
    //utility
    getImageTypeFromBase64(imgBase64){
        if (imgBase64.indexOf('data:image/png') >= 0) {
    
            imgBase64 = imgBase64.replace('data:image/png;base64,', '');
    
            return { type: 'png', imgBase64: imgBase64 };
    
        } else if (imgBase64.indexOf('data:image/jpeg') >= 0) {
    
            imgBase64 = imgBase64.replace('data:image/jpeg;base64,', '');
    
            return { type: 'jpg', imgBase64: imgBase64 };
    
        } else if (imgBase64.indexOf('data:image/jpg') >= 0) {
    
            imgBase64 = imgBase64.replace('data:image/jpg;base64,', '');
    
            return { type: 'jpg', imgBase64: imgBase64 };
    
        } else if (imgBase64.indexOf('data:image/gif') >= 0) {
            imgBase64 = imgBase64.replace('data:image/gif;base64,', '');
    
            return { type: 'gif', imgBase64: imgBase64 };
    
        } else {
            return { type: '', imgBase64: '' };
        }
    };
};

module.exports = Storage;


