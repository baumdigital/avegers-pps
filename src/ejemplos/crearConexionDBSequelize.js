/* eslint-disable max-len */
'use strict';

const Sequelize = require('sequelize');

/**
 * Función para inicializar una conexion db de sequelize
 * @param {Object} configDatabase [Es un objeto con la configuracion de la base de datos]
 * @param {string} timezoneSequelize [Es un string con el timezone que va a utilzar por default la db]
 * @returns {Object}
 */
const sequelize = (configDatabase, timezoneSequelize) => new Sequelize(configDatabase.database, configDatabase.username, configDatabase.password, {
  dialectOptions: {decimalNumbers: true},
  dialect: 'mysql', // la base de datos que se va a utilizar, puede ser postgres,mariadb etc
  timezone: timezoneSequelize,
  port: configDatabase.port,
  replication: {
    write: {
      host: configDatabase.replicas.write.host,
      pool: {
        maxConnections: 20,
        maxIdleTime: 30000,
      },
    },
    read: [{
      host: configDatabase.replicas.read.host,
      pool: {
        maxConnections: 20,
        maxIdleTime: 30000,
      },
    }],
  },
});

module.exports = sequelize;
