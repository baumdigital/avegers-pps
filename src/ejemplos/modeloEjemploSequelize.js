/* eslint-disable max-len */
/* eslint-disable indent */
/* eslint-disable camelcase */
'use strict';
const Sequelize = require('sequelize');
/**
 * Función para inicializar un modelo de sequelize
 * @param {Object} sequelize [Instancia de sequelize, es decir, la conexion a base de datos]
 * @param {Object} _DataTypes [Los dataTypes que soporta sequelize, es opcional]
 * @returns {Object}
 */
const init = (sequelize, _DataTypes) => {
  let DataTypes = _DataTypes ? _DataTypes : Sequelize.DataTypes;
  const Admin = sequelize.define('AdminModel', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    idRol: { // select id_rol as idRol from administradores
      type: DataTypes.INTEGER,
      defaultValue: null,
      field: 'id_rol',
    },
    usuario: {
      type: DataTypes.STRING,
      defaultValue: null,
      field: 'usuario',
    },
    password: {
      type: DataTypes.STRING,
      defaultValue: null,
      field: 'password',
    },
    nombre: {
      type: DataTypes.STRING,
      defaultValue: null,
      field: 'nombre',
    },
    apellidos: {
      type: DataTypes.STRING,
      defaultValue: null,
      field: 'apellidos',
    },
    email: {
      type: DataTypes.STRING,
      defaultValue: null,
      field: 'email',
    },
    imagen: {
      type: DataTypes.STRING,
      defaultValue: null,
      field: 'imagen',
    },
    estado: {
      type: DataTypes.BOOLEAN,
      defaultValue: 1,
      field: 'estado',
    },
  }, {
      // don't add the timestamp attributes (updatedAt, createdAt)
      timestamps: false,

      // don't delete database entries but set the newly added attribute deletedAt
      // to the current date (when deletion was done). paranoid will only work if
      // timestamps are enabled
      // paranoid: true,

      // don't use camelcase for automatically added attributes but underscore style
      // so updatedAt will be updated_at
      underscored: true,
      // disable the modification of table names; By default, sequelize will automatically
      // transform all passed model names (first parameter of define) into plural.
      // if you don't want that, set the following
      freezeTableName: true,
      // define the table's name
      tableName: 'administradores',
    });

  if (Admin) {
    /**
     * Función para hacer las relaciones de este nideki
     * @param {Object} db [Instancia de sequelize, es decir, la conexion a base de datos, la instancia de sequelize tiene todos los modelos ya inicializados]
     * @returns {undefined}
     */
    Admin.associate = (db) => {
      // los modelos se encuentran en db.models , es un objeto y los modelos se encuentran con el alias que uno les haya puesto
      // Por ejemplo este modelo se llamaria db.models.AdminModel
    };
  }
  return Admin;
};

module.exports = init;
