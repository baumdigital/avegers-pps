const StorageHelper = require('@baumdigital/storage-helper');
const s3Config = {
    url: process.env.S3_URL,
    version:  process.env.S3_VERSION,
    bucket: process.env.S3_BUCKET,
};
const awsSDK = {
  roleArn: process.env.ROLE_ARN,
  accessKeyId: process.env.AWS_SDK_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SDK_SECRET_ACCESS_KEY,
  region: process.env.AWS_SDK_REGION,
};

const Storage = new StorageHelper(s3Config, awsSDK);

async function urlImage(imagen, id, nombre) {  
  var data = await Storage.imageBase64toUrlS3(imagen, `${id}-${nombre}`, 'avengers/');   
  return data.result;
}

module.exports.urlImage = urlImage;