const Sequelize = require('sequelize');








const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  dialectOptions: { decimalNumbers: true },
  dialect: 'mysql',
  timezone: '-06:00',
  port: process.env.DB_PORT,
  replication: {
    write: {
      host: process.env.DB_WRITE_HOST,
      pool: {
        maxConnections: 20,
        maxIdleTime: 30000,
      },
    },
    read: [{
      host: process.env.DB_READ_HOST,
      pool: {
        maxConnections: 20,
        maxIdleTime: 30000,
      },
    }],
  },
});

module.exports = sequelize;