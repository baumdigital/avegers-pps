const db = require('../config/database');
'use strict';
const Sequelize = require('sequelize');

  const Genero = db.define('GeneroModel', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    id_label: {
      type: Sequelize.INTEGER,
      defaultValue: null,
      field: 'id_label',
    },
  }, {
      timestamps: false,
      underscored: true,
      freezeTableName: true,
      tableName: 'genero',
    });

  if (Genero) {
    Genero.associate = (db) => {

    };
  }

module.exports = Genero;

