const mysql = require('../config/database');
'use strict';
const Sequelize = require('sequelize');


  const Sys_label_idioma = mysql.define('Sys_label_idiomaModel', {
    id_label: {
      type: Sequelize.STRING,
      primaryKey: true,
      autoIncrement: true,
      field: 'id_label',
    },
    id_idioma: {
      type: Sequelize.STRING,
      defaultValue: null,
      field: 'id_idioma',
    },
    traduccion: {
        type: Sequelize.INTEGER,
        defaultValue: null,
        field: 'traduccion'
      }
  }, {
      timestamps: false,
      underscored: true,
      freezeTableName: true,
      tableName: 'sys_label_idioma',
    });

    if (Sys_label_idioma) {
        Sys_label_idioma.associate = (mysql) => {
        Sys_label_idioma.hasMany(mysql.sys_label_idioma.Sys_label_idiomaModel,{})
    };
  }

module.exports = Sys_label_idioma;

