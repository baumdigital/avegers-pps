const mysql = require('../config/database');
'use strict';
const Sequelize = require('sequelize');


  const Sys_idioma = mysql.define('Sys_idiomaModel', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    nombre: {
      type: Sequelize.STRING,
      defaultValue: null,
      field: 'nombre',
    },
    estado: {
        type: Sequelize.INTEGER,
        defaultValue: null,
        field: 'estado',
      }
  }, {
      timestamps: false,
      underscored: true,
      freezeTableName: true,
      tableName: 'sys_idioma',
    });

    if (Sys_idioma) {
        Sys_idioma.associate = (mysql) => {
        Sys_idioma.hasMany(mysql.sys_idioma.Sys_idiomaModel,{})
    };
  }

module.exports = Sys_idioma;

