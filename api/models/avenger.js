const mysql = require('../config/database');
'use strict';
const Sequelize = require('sequelize');
const Genero = require('../models/genero');

const Avenger = mysql.define('AvengerModel', {
  id: {
    type: Sequelize.INTEGER,
    required: true,
    primaryKey: true,
    autoIncrement: true,
    field: 'id'
  },
  nombre: {
    type: Sequelize.STRING,
    required: true,
    defaultValue: null,
    field: 'nombre'
  },
  edad: {
    type: Sequelize.INTEGER,
    required: true,
    defaultValue: null,
    field: 'edad'
  },
  idGenero: [{
    type: Sequelize,
    ref: Genero,
    field: 'id_genero'
  }],
  imagen: {
    type: Sequelize.STRING,
    defaultValue: null,
    field: 'imagen'
  }
},{
    timestamps: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'avengers',
  });

if (Avenger) {
  Avenger.associate = (mysql) => {
    Avenger.belongsTo(mysql.models.GeneroModel, {
      as: 'avengerGenero',
      foreignKey: 'id_genero',
      id: 'id'
    })
  };
}

module.exports = Avenger;

