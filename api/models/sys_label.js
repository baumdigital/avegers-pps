const mysql = require('../config/database');
'use strict';
const Sequelize = require('sequelize');


  const Sys_label = mysql.define('Sys_labelModel', {
    id: {
      type: Sequelize.STRING,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    }
  }, {
      timestamps: false,
      underscored: true,
      freezeTableName: true,
      tableName: 'sys_label',
    });

    if (Sys_label) {
        Sys_label.associate = (mysql) => {
            Sys_label.hasMany(mysql.sys_label.Sys_labelModel,{})
    };
  }

module.exports = Sys_label;

