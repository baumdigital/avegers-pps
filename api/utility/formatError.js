const R = require('ramda');
const formatJoiError = (joiError, objName) => {
    if (!joiError.isJoi) {
        return { mensaje: 'Something' };
    }

    if (!Array.isArray(joiError.details)) {
        return { mensaje: 'Something' };
    }

    const params = joiError.details.reduce((report, error, i) => {
        let path;
        if (error.path == 0) {
            if (Array.isArray(error.context.present)) {
                path = error.context.present.reduce((r, e, i) => {
                    return { ...r, [e]: error.message };
                }, {});
                return Object.assign({}, path, report);
            }
            return error.message;
        } else {
            path = error.path.reduceRight(
                (r, e, i, array) => ({
                    [e]: i == array.length - 1 ? error.message : r
                }),
                {}
            );
            return R.mergeDeepLeft(path, report); //({ path, report })
        }
    }, {});

    return { valid: false, error: { params: objName ? { [objName]: params } : params } };
};

module.exports = formatJoiError;