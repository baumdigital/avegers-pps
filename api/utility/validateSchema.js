const Joi = require('@hapi/joi');
const formatJoiError = require('./formatError');
const validateObj = (obj, schema, otherErrors = [], options = {}) => {  // todas los objetos en el array deben tener esta estructura { parametros: { [key]: 'error blabla'  }, isError} || {}
    if (typeof options !== 'object') {
        throw new Error('Invalid options, options must be an object');
    }
    // siempre hay que mandar la opcion de language si queremos que salgan los errores como tenemos de standar
    options = { ...{ abortEarly: false, language: JoiLanguaje, stripUnknown: true, convert: true }, ...options };
    const resultValidate = Joi.validate(obj, schema, options);
    if (resultValidate.error) {
        resultValidate.errorJoi = resultValidate.error;
        resultValidate.error = formatJoiError(resultValidate.error);
    }
    const extras = otherErrors.reduce((result, p, i) => {
        const resultExecution = p;
        const { isError, parametros } = resultExecution;
        return { error: { ...result.error, parametros }, hasError: ((isError) ? true : result.hasError) };
    }, { error: {}, hasError: false })

    if (extras.hasError && resultValidate.error) {
        const aux1 = resultValidate.error.error.parametros;
        const aux2 = extras.error.parametros;
        resultValidate.error.error.parametros = { ...aux2, ...aux1 };
    }
    if (extras.hasError && !resultValidate.error) {
        resultValidate.error = {
            valido: false,
            parametros: extras.error.parametros
        }
    }
    return resultValidate;
};

//to not preceeded strings by the key name use !! before the error
const preceeded = '!!'

const JoiLanguaje = {
    root: 'value',
    key: '"{{!label}}" ',
    messages: {
        wrapArrays: true
    },
    any: {
        unknown: preceeded + 'no es permitido',
        invalid: preceeded + 'contiene un valor inválido',
        empty: preceeded + 'no puede ser vacío',
        required: preceeded + 'es requerido',
        allowOnly: preceeded + 'debe ser uno de estos valores {{valids}}',
        default: preceeded + 'threw an error when running default method',
    },
    alternatives: {
        base: preceeded + 'not matching any of the allowed alternatives',
        child: null
    },
    array: {
        base: preceeded + 'must be an array',
        includes: preceeded + 'at position {{pos}} does not match any of the allowed types',
        includesSingle: preceeded + 'single value of "{{!label}}" does not match any of the allowed types',
        includesOne: preceeded + 'at position {{pos}} fails because {{reason}}',
        includesOneSingle: preceeded + 'single value of "{{!label}}" fails because {{reason}}',
        includesRequiredUnknowns: preceeded + 'does not contain {{unknownMisses}} required value(s)',
        includesRequiredKnowns: preceeded + 'does not contain {{knownMisses}}',
        includesRequiredBoth: preceeded + 'does not contain {{knownMisses}} and {{unknownMisses}} other required value(s)',
        excludes: preceeded + 'at position {{pos}} contains an excluded value',
        excludesSingle: preceeded + 'single value of "{{!label}}" contains an excluded value',
        hasKnown: preceeded + 'does not contain at least one required match for type "{{!patternLabel}}"',
        hasUnknown: preceeded + 'does not contain at least one required match',
        min: preceeded + 'must contain at least {{limit}} items',
        max: preceeded + 'must contain less than or equal to {{limit}} items',
        length: preceeded + 'must contain {{limit}} items',
        ordered: preceeded + 'at position {{pos}} fails because {{reason}}',
        orderedLength: preceeded + 'at position {{pos}} fails because array must contain at most {{limit}} items',
        ref: preceeded + 'references "{{ref}}" which is not a positive integer',
        sparse: preceeded + 'must not be a sparse array',
        unique: preceeded + 'position {{pos}} contains a duplicate value'
    },
    boolean: {
        base: preceeded + 'deber ser un boolean'
    },
    binary: {
        base: preceeded + 'must be a buffer or a string',
        min: preceeded + 'must be at least {{limit}} bytes',
        max: preceeded + 'must be less than or equal to {{limit}} bytes',
        length: preceeded + 'must be {{limit}} bytes'
    },
    date: {
        base: preceeded + 'must be a number of milliseconds or valid date string',
        strict: preceeded + 'must be a valid date',
        min: preceeded + 'must be larger than or equal to "{{limit}}"',
        max: preceeded + 'must be less than or equal to "{{limit}}"',
        less: preceeded + 'must be less than "{{limit}}"',
        greater: preceeded + 'must be greater than "{{limit}}"',
        isoDate: preceeded + 'must be a valid ISO 8601 date',
        timestamp: {
            javascript: preceeded + 'must be a valid timestamp or number of milliseconds',
            unix: preceeded + 'must be a valid timestamp or number of seconds'
        },
        ref: preceeded + 'references "{{ref}}" which is not a date'
    },
    function: {
        base: preceeded + 'must be a Function',
        arity: preceeded + 'must have an arity of {{n}}',
        minArity: preceeded + 'must have an arity greater or equal to {{n}}',
        maxArity: preceeded + 'must have an arity lesser or equal to {{n}}',
        ref: preceeded + 'must be a Joi reference',
        class: preceeded + 'must be a class'
    },
    lazy: {
        base: '!!schema error: lazy schema must be set',
        schema: '!!schema error: lazy schema function must return a schema'
    },
    object: {
        base: preceeded + 'must be an object',
        child: '!!child "{{!child}}" fails because {{reason}}',
        min: preceeded + 'must have at least {{limit}} children',
        max: preceeded + 'must have less than or equal to {{limit}} children',
        length: preceeded + 'must have {{limit}} children',
        allowUnknown: '!!"{{!child}}" is not allowed',
        with: '!!"{{mainWithLabel}}" missing required peer "{{peerWithLabel}}"',
        without: '!!"{{mainWithLabel}}" conflict with forbidden peer "{{peerWithLabel}}"',
        missing: preceeded + 'must contain at least one of {{peersWithLabels}}',
        xor: preceeded + 'contains a conflict between exclusive peers {{peersWithLabels}}',
        oxor: preceeded + 'contains a conflict between optional exclusive peers {{peersWithLabels}}',
        and: preceeded + 'contains {{presentWithLabels}} without its required peers {{missingWithLabels}}',
        nand: '!!"{{mainWithLabel}}" must not exist simultaneously with {{peersWithLabels}}',
        assert: '!!"{{ref}}" validation failed because "{{ref}}" failed to {{message}}',
        rename: {
            multiple: preceeded + 'cannot rename child "{{from}}" because multiple renames are disabled and another key was already renamed to "{{to}}"',
            override: preceeded + 'cannot rename child "{{from}}" because override is disabled and target "{{to}}" exists',
            regex: {
                multiple: preceeded + 'cannot rename children {{from}} because multiple renames are disabled and another key was already renamed to "{{to}}"',
                override: preceeded + 'cannot rename children {{from}} because override is disabled and target "{{to}}" exists'
            }
        },
        type: preceeded + 'must be an instance of "{{type}}"',
        schema: preceeded + 'must be a Joi instance'
    },
    number: {
        base: preceeded + 'must be a number',
        unsafe: preceeded + 'must be a safe number',
        min: preceeded + 'must be larger than or equal to {{limit}}',
        max: preceeded + 'must be less than or equal to {{limit}}',
        less: preceeded + 'must be less than {{limit}}',
        greater: preceeded + 'must be greater than {{limit}}',
        integer: preceeded + 'must be an integer',
        negative: preceeded + 'must be a negative number',
        positive: preceeded + 'must be a positive number',
        precision: preceeded + 'must have no more than {{limit}} decimal places',
        ref: preceeded + 'references "{{ref}}" which is not a number',
        multiple: preceeded + 'must be a multiple of {{multiple}}',
        port: preceeded + 'must be a valid port',
    },
    string: {
        base: preceeded + 'must be a string',
        min: preceeded + 'length must be at least {{limit}} characters long',
        max: preceeded + 'length must be less than or equal to {{limit}} characters long',
        length: preceeded + 'length must be {{limit}} characters long',
        alphanum: preceeded + 'must only contain alpha-numeric characters',
        token: preceeded + 'must only contain alpha-numeric and underscore characters',
        regex: {
            base: preceeded + 'with value "{{!value}}" fails to match the required pattern: {{pattern}}',
            name: preceeded + 'with value "{{!value}}" fails to match the {{name}} pattern',
            invert: {
                base: preceeded + 'with value "{{!value}}" matches the inverted pattern: {{pattern}}',
                name: preceeded + 'with value "{{!value}}" matches the inverted {{name}} pattern'
            }
        },
        email: preceeded + 'debe ser un email válido',
        uri: preceeded + 'must be a valid uri',
        uriRelativeOnly: preceeded + 'must be a valid relative uri',
        uriCustomScheme: preceeded + 'must be a valid uri with a scheme matching the {{scheme}} pattern',
        isoDate: preceeded + 'must be a valid ISO 8601 date',
        guid: preceeded + 'must be a valid GUID',
        hex: preceeded + 'must only contain hexadecimal characters',
        hexAlign: preceeded + 'hex decoded representation must be byte aligned',
        base64: preceeded + 'must be a valid base64 string',
        dataUri: preceeded + 'must be a valid dataUri string',
        hostname: preceeded + 'must be a valid hostname',
        normalize: preceeded + 'must be unicode normalized in the {{form}} form',
        lowercase: preceeded + 'must only contain lowercase characters',
        uppercase: preceeded + 'must only contain uppercase characters',
        trim: preceeded + 'must not have leading or trailing whitespace',
        creditCard: preceeded + 'must be a credit card',
        ref: preceeded + 'references "{{ref}}" which is not a number',
        ip: preceeded + 'must be a valid ip address with a {{cidr}} CIDR',
        ipVersion: preceeded + 'must be a valid ip address of one of the following versions {{version}} with a {{cidr}} CIDR',
        phonenumber: preceeded + 'did not seem to be a phone number',
    },
    symbol: {
        base: preceeded + 'must be a symbol',
        map: preceeded + 'must be one of {{map}}'
    }
};

module.exports = validateObj;

