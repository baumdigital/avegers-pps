const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Genero = require('../models/genero')


//const Op = Sequelize.Op;

router.get('/', async (req, res) => {
    try {
        const data = await Genero.findAll()
        res.json(data);
    } catch (error) {
        res.status(500);
        res.send(`INFO: ${error}`);
    }
});


module.exports = router; 