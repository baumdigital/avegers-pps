const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Avenger = require('../models/avenger');
const Genero = require('../models/genero');
const Sequelize = require('sequelize');
const Ramda = require('ramda');
const { validateObj } = require('../utility');

Avenger.associate(db);
Genero.associate(db);
require('dotenv').config();

const Joi = require('@hapi/joi');
const admin = require('../middleware/admin');
const Language = require('../middleware/language');
const fs = require('fs');
const s3 = require('../s3/s3');

const Op = Sequelize.Op;

const idGeneroSchema = Joi.number().integer().valid([0, 1, 2]);
const schemaRequestGet = Joi.object({
    id: Joi.number().positive(),
    nombre: Joi.string().max(50),
    idGenero: idGeneroSchema,
    rangoEdad: Joi.array().items(Joi.number().integer().min(0)).min(2).max(2),
});

const schemaRequestPost = Joi.object({
    nombre: Joi.string().min(3).max(50),
    idGenero: idGeneroSchema,
    edad: Joi.number().positive(),
    imagen: Joi.string().dataUri()
});

const schemaRequestPut = Joi.object({
    id: Joi.number().positive(),
    nombre: Joi.string().min(3).max(50),
    idGenero: idGeneroSchema,
    edad: Joi.number().positive(),
    imagen: Joi.string().dataUri()
});

const schemaDelete = Joi.object({
    id: Joi.number().positive().integer().required()
});


//Search
router.get('/', [admin], async (req, res) => {
    try {
        const language = req.lang;
        const resultValidate = validateObj(req.query, schemaRequestGet, []);
        if (resultValidate.error) {
            res.statusMessage = 'Bad request';
            res.status(400);
            res.json(resultValidate.error);
            return false;
        };
        const {
            id,
            nombre,
            rangoEdad,
            idGenero
        } = resultValidate.value;
        const where = {
            [Op.and]: []
        };
        if (id) {
            where[Op.and].push({
                id: { [Op.eq]: id }
            })
        }
        if (nombre) {
            where[Op.and].push({
                nombre: { [Op.like]: `%${nombre}%` }
            })
        }
        if (rangoEdad && Array.isArray(rangoEdad)) {
            where[Op.and].push({
                edad: { [Op.between]: rangoEdad }
            })
        }
        if (idGenero) {
            where[Op.and].push({
                idGenero: { [Op.eq]: idGenero == 0 ? null : idGenero }
            })
        }
        if(where[Op.and].length == 0){
            where[Op.and].push(Sequelize.literal('(1=1)'));
        }
        const data = await Avenger.findAll({
            where,
            attributes: {
                include: [
                    [Sequelize.fn("concat", process.env.S3_URL, Sequelize.col("imagen")),'imagen'],
                    [Sequelize.literal(`(Select traduccion from sys_label_idioma where sys_label_idioma.id_label = avengerGenero.id_label  AND id_idioma = '${language}' limit 1)`), 'genero']
                ],
                exclude: ['id_genero']
            },
            include: [{
                required: false,
                model: Genero,
                as: 'avengerGenero',
                attributes: []
            }]
        });
        res.status(200);
        res.json({
            valido: true,
            data
        });
    } catch (error) {
        console.log(error);
        res.status(500);
        res.json({
            valido: true,
            error: {
                message: error,
            }
        });
    }
});

//Add
router.post('/', [admin], async (req, res) => {
    const language = req.lang;
    const resultValidate = validateObj(req.body, schemaRequestPost, []);
    if (resultValidate.error) {
        res.statusMessage = 'Bad request';
        res.status(400);
        res.json(resultValidate.error);
        return false;
    };
    const {
        nombre,
        edad,
        idGenero,
        imagen,
    } = resultValidate.value;
    let data = new Avenger({
        nombre: nombre,
        edad: edad,
        idGenero: idGenero == 0 ? null : idGenero,
        imagen: null
    });
    await data.save();
    const resUrlImage = await s3.urlImage(imagen,data.id,nombre);
    await data.update({
        imagen: resUrlImage
    })
    res.status(201);
    res.json({
        valido: true,
        data: await loadAvenger(data.id, language)
    });
});

//Update
router.put('/', [admin], async (req, res) => {
    try {
        const language = req.lang;
        const resultValidate = validateObj(req.body, schemaRequestPut, []);
        if (resultValidate.error) {
            res.statusMessage = 'Bad request';
            res.status(400);
            res.json(resultValidate.error);
            return false;
        };
        const {
            id,
            nombre,
            edad,
            idGenero,
            imagen,
        } = resultValidate.value;
        const data = await Avenger.findOne({
            where: {
                id: id
            }
        })
        if (!data) {//no existe Avenger
            res.status(404);
            res.json({
                valido: false,
                error: {
                    message: "Not Found",
                }
            })
        } else if (data) {//existe Avenger
            const resUrlImage = await s3.urlImage(imagen, id, nombre);
            await data.update({
                nombre: nombre,
                edad: edad,
                idGenero: idGenero == 0 ? null : idGenero,
                imagen: resUrlImage
            })
            res.status(200);
            const response = {
                valido: true,
                data: await loadAvenger(data.id, language)
            };
            res.json(response);
        }
    } catch (error) {
        res.status(500);
        res.send(`INFO: ${error}`);
    }
});

//Delete
router.delete('/', [admin], async (req, res) => {
    try {
        const resultValidate = validateObj(req.query, schemaDelete, []);
        if (resultValidate.error) {
            res.statusMessage = 'Bad request';
            res.status(400);
            res.json(resultValidate.error);
            return false;
        };
        const { id } = resultValidate.value;
        const data = await Avenger.findOne({
            where: {
                id
            }
        })
        if (!data) {
            res.status(404);
            res.send({
                valido: false,
                error: {
                    message: "Not Found",
                    parameters: {}
                }
            })
        } else if (data) {
            await data.destroy();
            res.status(200);
            res.json({
                valido: true
            });
        }
    } catch (error) {
        res.status(500);
        res.send(`INFO: ${error}`);
    }
});

const loadAvenger = async (id, language = 'es') => {
    try {
        const data = await Avenger.findOne({
            where: { id },
            attributes: {
                include: [
                    [Sequelize.fn("concat", process.env.S3_URL, Sequelize.col("imagen")),'imagen'],
                    [Sequelize.literal(`(Select traduccion from sys_label_idioma where sys_label_idioma.id_label = avengerGenero.id_label  AND id_idioma = '${language}' limit 1)`), 'genero']
                ],
                exclude: ['id_genero','imagen']
            },
            include: [{
                required: false,
                model: Genero,
                as: 'avengerGenero',
                attributes: []
            }]
        })
        return data ? data.toJSON() : null;
    } catch (error) {
        console.log(`[loadAvenger.error] => ${error}`);
        return null;
    }
}

module.exports = router;

