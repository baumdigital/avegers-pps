const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Sys_idioma = require('../models/sys_idioma')


//const Op = Sequelize.Op;

router.get('/', async (req, res) => {
    try {
        const data = await Sys_idioma.findAll()
        res.json(data);
    } catch (error) {
        res.status(500);
        res.send(`INFO: ${error}`);
    }
});


module.exports = router; 