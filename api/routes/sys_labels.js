const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Sys_label = require('../models/sys_label')


//const Op = Sequelize.Op;

router.get('/', async (req, res) => {
    try {
        const data = await Sys_label.findAll()
        res.json(data);
    } catch (error) {
        res.status(500);
        res.send(`INFO: ${error}`);
    }
});


module.exports = router; 