module.exports = function (req, res, next){
    if(req.headers['x-api-key'] != process.env.APIKEY) {
      console.log(process.env.APIKEY);
      
        res.status(403)
        res.json({
            "valid": false,
            "error": {
              "message": 'Access denided...',
            }
          })
        return 
    }
    next();
}