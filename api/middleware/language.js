function myLanguage(req){
  let language = req.headers['accept-language'];
    if(language != 'en') {
        language = 'es';
    }
    return language;
}

module.exports.myLanguage = myLanguage;
